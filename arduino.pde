/**
 * @file   arduino.pde
 * @author Ганжа Д.Е., гр. 515B
 * @date   21 июня 2019
 * @brief  Работа по практике 
 *
 * Разработка устройства контроля уровня рабочей жидкости в баке
 */
 
//подключаем необходмые библиотеки 
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <iarduino_RTC.h>

//инициализируем те пины, которые будут задействованы в ходе работы программы
#define Trig 8 
#define Echo 9
#define Music 7
#define Gre 6
#define Yel 5
#define Red 4 
#define r 34
#define g 36
#define b 38

//инициализируем переменные расстояния и времени импульса, подаваемого на датчик HC-SR04
unsigned int impulseTime=0; 
unsigned int distance_sm=0;

//задаем параметры для LCD дисплея
LiquidCrystal_I2C lcd(0x27,20,4);

//инициализируем пины, которые будут отвечать за корректное отображение времени в программе 
iarduino_RTC time(RTC_DS1302, 10, 13, 12);

//с помощью функции setup() мы передаем  микроконтроллеру команды, которые он выполнит в момент загрузки
void setup()
{
  pinMode(r,INPUT);
  pinMode(g,INPUT);
  pinMode(b,INPUT);
  delay(300);//задержка в 300мс
  Serial.begin(9600);//задаем скорость передачи данных
  time.begin();//подключаем функцию для отображения времени
  //обозначаем какой пин будет принимать, а какой будет получать данные
  pinMode (Trig, OUTPUT);
  pinMode (Music, OUTPUT);  
  pinMode (Gre, OUTPUT);  
  pinMode (Yel, OUTPUT); 
  pinMode (Red, OUTPUT); 
  pinMode (Echo, INPUT);   
  lcd.init();//инициализируем дисплей                       
  lcd.backlight();//подключаем подсветку дисплея
  lcd.setCursor(4,1);//ставим курсор на нужные нам координаты
  lcd.print("Distance is:");//выводим на дисплей
  lcd.setCursor(3,2);//ставим курсор на нужные нам координаты
}

void loop()
{ 
  digitalWrite(r,HIGH);
  digitalWrite(g,HIGH);
  digitalWrite(b,HIGH);
  lcd.setCursor(8,0);//ставим курсор на нужные нам координаты
  time.gettime();//считываем текуще время
  lcd.print(time.Hours);lcd.print(":");lcd.print(time.minutes);//выводим на время на дисплей
  if(time.seconds==59)//если текущий показатель секунд равен 59 то мы обновляем цифры времени на экране, чтобы избежать неккоректной работы программы
  {
    lcd.setCursor(0,0);//ставим курсор на нужные нам координаты
    lcd.print("                    ");//очищаем часть экрана со временем 
  }
  delay(1); // приостанавливаем на 1 мс, чтобы не выводить время несколько раз за 1мс 
  digitalWrite (Trig, HIGH);//Подаем сигнал на порт, подключенный к ультравуковому датчику чтобы пустить импульс
  delayMicroseconds (10);//пускаем импульс длинной в 10мс  
  digitalWrite (Trig, LOW);//отключаем сигнал 
  impulseTime=pulseIn (Echo, HIGH);//принимаем время, которое потребовалось чтобы импульс дошел обратно  
  distance_sm =impulseTime/58;//считаем расстояние чеез полученное время, которое прошел импульс
  lcd.setCursor(3,2);//ставим курсор на нужные нам координаты
  lcd.print(distance_sm);//выводим на дисплей расстояние 
  lcd.print(" centimetres  ");//выводим на дисплей 
  lcd.setCursor(0,3);//ставим курсор на нужные нам координаты
  if(distance_sm<=100&distance_sm>90)
  {
    lcd.print("||                  ");//выводим шкалу заполнения бака
    tone (Music, 200);//включаем звуковой сигнал
    digitalWrite (Gre, HIGH);//включаем светодиод
  }
  else if(distance_sm>100)
  {
    noTone(Music);//выключаем звуковой сигнал
    digitalWrite (Gre, LOW);//выключаем светодиод
    digitalWrite (Yel, LOW);//выключаем светодиод
    digitalWrite (Red, LOW);//выключаем светодиод
    lcd.print("                    ");//выводим шкалу заполнения бака
  }
  if(distance_sm<=90&distance_sm>80)
  {
    lcd.setCursor(0,3);//ставим курсор на нужные нам координаты
    lcd.print("||||                ");//выводим шкалу заполнения бака
    tone (Music, 300);//включаем звуковой сигнал
    digitalWrite (Red, LOW);//выключаем светодиод
    digitalWrite (Yel, LOW);//выключаем светодиод
    digitalWrite (Gre, HIGH);//включаем светодиод
  }
  if(distance_sm<=80&distance_sm>70)
  {
    lcd.setCursor(0,3);//ставим курсор на нужные нам координаты
    lcd.print("||||||              ");//выводим шкалу заполнения бака
    tone (Music, 400);//включаем звуковой сигнал
    digitalWrite (Red, LOW);//выключаем светодиод
    digitalWrite (Yel, LOW);//выключаем светодиод
    digitalWrite (Gre, HIGH);//включаем светодиод
  }
  if(distance_sm<=70&distance_sm>60)
  {
    lcd.setCursor(0,3);//ставим курсор на нужные нам координаты
    lcd.print("||||||||            ");//выводим шкалу заполнения бака
    tone (Music, 500);//включаем звуковой сигнал
    digitalWrite (Red, LOW);//выключаем светодиод
    digitalWrite (Yel, LOW);//выключаем светодиод
    digitalWrite (Gre, HIGH);//включаем светодиод
  }
  if(distance_sm<=60&distance_sm>50)
  {
    lcd.setCursor(0,3);//ставим курсор на нужные нам координаты
    lcd.print("||||||||||          ");//выводим шкалу заполнения бака
    tone (Music, 600);//включаем звуковой сигнал
    digitalWrite (Red, LOW);//выключаем светодиод
    digitalWrite (Yel, LOW);//выключаем светодиод
    digitalWrite (Gre, HIGH);//включаем светодиод
  }
  if(distance_sm<=50&distance_sm>40)
  {
    lcd.setCursor(0,3);//ставим курсор на нужные нам координаты
    lcd.print("||||||||||||        ");//выводим шкалу заполнения бака
    tone (Music, 900);//включаем звуковой сигнал
    digitalWrite (Red, LOW);//выключаем светодиод
    digitalWrite (Gre, HIGH);//включаем светодиод
    digitalWrite (Yel, HIGH);//включаем светодиод
  }
  if(distance_sm<=40&distance_sm>30)
  {
    lcd.setCursor(0,3);//ставим курсор на нужные нам координаты
    lcd.print("||||||||||||||      ");//выводим шкалу заполнения бака
    tone (Music, 1100);//включаем звуковой сигнал
    digitalWrite (Red, LOW);//выключаем светодиод
    digitalWrite (Gre, HIGH);//включаем светодиод
    digitalWrite (Yel, HIGH);//включаем светодиод
  }
  if(distance_sm<=30&distance_sm>20)
  {
    lcd.setCursor(0,3);//ставим курсор на нужные нам координаты
    lcd.print("||||||||||||||||    ");//выводим шкалу заполнения бака
    tone (Music, 1300);//включаем звуковой сигнал
    digitalWrite (Red, LOW);//выключаем светодиод
    digitalWrite (Gre, HIGH);//включаем светодиод
    digitalWrite (Yel, HIGH);//включаем светодиод
  }
  if(distance_sm<=20&distance_sm>10)
  {
    lcd.setCursor(0,3);//ставим курсор на нужные нам координаты
    lcd.print("||||||||||||||||||  ");//выводим шкалу заполнения бака
    tone (Music, 1500);//включаем звуковой сигнал
    digitalWrite (Gre, HIGH);//включаем светодиод
    digitalWrite (Yel, HIGH);//включаем светодиод
    digitalWrite (Red, HIGH);//включаем светодиод
  }
  if(distance_sm<=10)
  {
    lcd.setCursor(0,3);//ставим курсор на нужные нам координаты
    lcd.print("||||||||||||||||||||");//выводим шкалу заполнения бака
    tone (Music, 1700);//включаем звуковой сигнал
    digitalWrite (Gre, HIGH);//включаем светодиод
    digitalWrite (Yel, HIGH);//включаем светодиод
    digitalWrite (Red, HIGH);//включаем светодиод
  }
  delay(400);//делаем паузу в 400мс
}